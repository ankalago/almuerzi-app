import {useEffect, useState} from 'react'

const useFetch = (url) => {
    const [loading, setLoading] = useState(true)
    const [data, setData] = useState(true)

    const getFetch = async () => {
        const response = await fetch(url)
        const data = await response.json();
        setLoading(false)
        setData(data)
    }

    useEffect(() => {
        getFetch()
    }, [])

    return {loading, data}
}

export default useFetch
