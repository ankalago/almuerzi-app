import {createAppContainer, createSwitchNavigator} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import MealsScreen from './screens/MealsScreen';
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import AuthLoading from './screens/AuthLoading';
import ModalScreen from './screens/ModalScreen';

const onBoardingNavigator = createStackNavigator({
  Login: LoginScreen,
  Register: RegisterScreen
}, {
  initialRouteName: 'Login'
})

const AppNavigator = createStackNavigator({
  Meals: {
    screen: MealsScreen
  }
}, {
  initialRouteName: 'Meals'
})

const RootStack =  createStackNavigator({
  Main: AppNavigator,
  Modal: ModalScreen
}, {
  mode: 'modal',
  headerMode: 'none'
})

const BaseStack = createSwitchNavigator({
  AuthLoading,
  OnBoarding: onBoardingNavigator,
  Root: RootStack
}, {
  initialRouteName: 'AuthLoading'
})

export default createAppContainer(BaseStack)
