import React from 'react'
import {AsyncStorage, View, StyleSheet, TextInput, Text, Button, Alert} from 'react-native';
import useForm from '../hooks/useForm';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 15
    },
    title: {
        fontSize: 24,
    },
    input: {
        height: 40,
        borderColor: '#ccc',
        borderWidth: 1,
        alignSelf: 'stretch',
        marginBottom: 10,
        paddingHorizontal: 5
    }
})

const Login = ({navigation}) => {

    const initialState = {
        email: '',
        password: ''
    }

    const onSubmit = (values) => {
        fetch(`https://serverless.paul-jacome.vercel.app/api/auth/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(values)
        })
            .then(x => x.text())
            .then(x => {
                try{
                    return JSON.parse(x)
                } catch (e) {
                    throw x
                }
            })
            .then(x => {
                AsyncStorage.setItem('token', x.token)
                navigation.navigate('Meals')
            })
            .catch(e => Alert.alert('Error', e))
    }

    const { subscribe, inputs, handleSubmit } = useForm(initialState, onSubmit)

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Iniciar Sesión</Text>
            <TextInput
                autoCapitalize='none'
                placeholder="correo electrónico"
                onChangeText={subscribe('email')}
                style={styles.input}
                value={inputs.email}
            />
            <TextInput
                autoCapitalize='none'
                placeholder="contraseña"
                onChangeText={subscribe('password')}
                style={styles.input}
                value={inputs.password}
                secureTextEntry={true}
            />
            <Button title="Iniciar sesión" onPress={handleSubmit} />
            <Button title="Registrarse" onPress={() => navigation.navigate('Register')} />
        </View>
    )
}

export default Login
