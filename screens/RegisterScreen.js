import React from 'react'
import {View, StyleSheet, TextInput, Text, Button, Alert} from 'react-native';
import useForm from '../hooks/useForm';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 15
    },
    title: {
        fontSize: 24,
    },
    input: {
        height: 40,
        borderColor: '#ccc',
        borderWidth: 1,
        alignSelf: 'stretch',
        marginBottom: 10,
        paddingHorizontal: 5
    }
})

const Register = ({navigation}) => {

    const initialState = {
        email: '',
        password: ''
    }

    const onSubmit = (values) => {
        fetch(`https://serverless.paul-jacome.vercel.app/api/auth/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(values)
        })
            .then(x => x.text())
            .then(x => {
                if(x === 'Usuario creado con exito') {
                    return Alert.alert(
                        'Exito',
                        x,
                        [{
                            text: 'Ir al inicio', onPress: () => navigation.navigate('Login')
                        }]
                    )
                }
                Alert.alert(
                    'Error',
                    x,
                )
            })
    }

    const { subscribe, inputs, handleSubmit } = useForm(initialState, onSubmit)

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Registro</Text>
            <TextInput
                autoCapitalize='none'
                placeholder="correo electrónico"
                style={styles.input}
                onChangeText={(e) => subscribe('email')(e)}
                value={inputs.email}
            />
            <TextInput
                autoCapitalize='none'
                placeholder="contraseña"
                style={styles.input}
                onChangeText={subscribe('password')}
                value={inputs.password}
                secureTextEntry={true}
            />
            <Button title="Enviar" onPress={handleSubmit} />
            <Button title="Volver" onPress={() => navigation.navigate('Login')} />
        </View>
    )
}

export default Register
