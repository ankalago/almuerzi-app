import React, {useEffect} from 'react'
import {View, AsyncStorage, ActivityIndicator} from 'react-native';

const AuthLoading = ({navigation}) => {

    useEffect(() => {
        AsyncStorage.getItem('token')
            .then(x => {
                navigation.navigate(x ? 'Root' : 'OnBoarding')
            })
    }, [])

    return (
        <View>
            <ActivityIndicator />
        </View>
    )
}

export default AuthLoading
